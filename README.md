# Energy Forecaster package description

This repo aims at giving the tools to perform the prediction:

	every hour 'h' we want to predict the integral value of our signal

It also contains the tools necessary to gather weather data from the provider: "Darksy". The full description is available on their website.
 
## Basic Use 1: Prediction

* first download this repo and run: python setup.py inside your virtual environnent

* get your historical (power) data as dataframe [time_col, power_col], and the weather history as [date_index, feature_1,...., feature_n] using weather_forecast.py tools

* compute:

```python

from eos_energy_forecaster.utils import process_data, update_preprocess, predict_preprocess

x_train, y_train, columns_order = process_data(signal_dataframe, time_col, power_col, integral_bounds, hour_of_pred, mean_lb=6, var_lb=15, weather_base=False)

# the optional parameters are explained later (or in the code itself)
```

* Define your predictor:

```python

from eos_energy_forecaster.knn_tools import Knn

predictor = Knn(n_neighbors=15)
predictor.fit(x_train, y_train)
# the optional parameters are explained later (or in the code itself)
```

Now you can:

* update your predictor as following:

```python
from eos_energy_forecaster.utils import update_preprocess

# suppose you have computed new_data_dict
new_data_array = update_preprocess(new_data_dict, new_y, columns_order)
```

* perform prediction
```python
from eos_energy_forecaster.utils import predict_preprocess

# suppose you have the following features dict: x_test_dict
x_test_array = predict_preprocess(x_test_dict, columns_order)

prediction = predictor.predict(x_test_array)
```

## Basic Use 2: forecaster gathering and storage

This repos also gives the tools to gather data from darsky web api.

When a (valid) requests is made on darksy api two "types" of data are given: daily and hourly. For the moment one has to choose which one to use.

There are two basic usage:

#### Get the data:

```python
import logging
from eos_energy_forecaster.weather_forecast import get_request, process_weather_dict, KEYS, LOGGER

lat = 47.983162
lon = -1.570272

raw_data = get_request(lat, lon, KEYS[0], request_date, LOGGER)
# unit is a default argument = "si" (internation system)

```


#### Get the data and store them:

two ways of storage have been developped:

* as a file in a folder
* in a database under the formalism: (timestamp, data_dictionnary)


```python

import logging
import datetime as dt

from eos_energy_forecaster.weather_forecast import weather_record, KEYS, LOGGER

start_record = dt.datetime(year=2018, month=1, day=1, hour=0)
end_record = dt.datetime(year=2018, month=9, day=1, hour=0)

lat = 46.870730
lon = 4.057105

# To record in base:
base_info = {"table_name": "table", "user": "user", "password": "password", "host": "host", "dbname": "dbname"}

weather_record(lat=lat, lon=lon, first_date=start_record, key=KEYS[0], last_date=end_record, mode="daily", delta_t="1D", file_path=None, save_base=True, save_file=False, logger=LOGGER, **base_info)

# To record in file system:

file_path = "/path_to_directory_ending_with/"

weather_record(lat=lat, lon=lon, first_date=start_record, key=KEYS[0], last_date=end_record, mode="daily", delta_t="1D", file_path=file_path, save_base=False, save_file=True, logger=LOGGER)
```


## How is this repository organised:



### 1) The prediction "machinery":


	knn_tools.py contains 3 major elements:

	   ______________
		Knn: [class]
	   ______________

			Compulsory argument:
			---------------------

				- n_neighbors [int]

			Optional:
			---------
				- weights [str] way of computing the average

			Method:
			-------

				fit

				update

				predict

		_____________________________
		feature selection: [function]
		_____________________________

			Returns the index of the best features selected as list


			Compulsory arguments:
			---------------------

			- x_train: [np.ndarray] of shape (nb_sample, nb_features)
			- y_train: [np.ndarray] of shape (nb_sample, pred_dim)

			- x_test: [np.ndarray] of shape (nb_sample, nb_features)
			- y_test: [np.ndarray] of shape (nb_sample, pred_dim)

			- max_nb_feat: [int] maximum number of features to be selected

			Optional:
			---------

            nb_neigh [int], (default 15) number of neighbours for features selection


		________________________
		optimize_nn: [function]
		________________________

		Aims at retrieving an "optimal" number of neighbours


		Compulsory arguments :
		----------------------
			- x_train: [np.ndarray] of shape (nb_sample, nb_features)
			- y_train: [np.ndarray] of shape (nb_sample, pred_dim)

			- x_test: [np.ndarray] of shape (nb_sample, nb_features)
			- y_test: [np.ndarray] of shape (nb_sample, pred_dim)

		
		Optional:
		---------
			n_limit=(2, 20)
			min_nn=5



2) The data processing Tools:
______________________________


	utils.py contains mainly specific tools in order to process data (compute features, checks on dimension, integral computation) related to our current problem: predicting an integral.

	It contains 




"""