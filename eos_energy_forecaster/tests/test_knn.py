# -*- coding: utf-8 -*-
"""knn.test_victron"""
import unittest
import numpy as np

from eos_energy_forecaster.knn_tools import Knn


class TestKNN(unittest.TestCase):
    def setUp(self):
        self.model_params_1 = {"n_neighbors": 5,
                               "weights": "uniform"}

        self.model_params_2 = {"n_neighbors": 5,
                               "weights": "distance"}

        self.knn_unif = Knn(**self.model_params_1)
        self.knn_dist = Knn(**self.model_params_2)

    def test_wrong_init(self):
        """
        assert an error is raised if the init arguments are not
        in the defined format
        """
        model_params_wrong_1 = {"n_neighbors": 5.5,
                                "weights": "uniform"}

        model_params_wrong_2 = {"n_neighbors": 5,
                                "weights": "patali"}

        self.assertRaises(TypeError, Knn, **model_params_wrong_1)
        self.assertRaises(ValueError, Knn, **model_params_wrong_2)

    def test_wrong_X_Y_shape(self):
        """
            assert that an error is raised if the number of sample
            is different in each X, Y
        """

        X_train_wrong = np.random.uniform(0, 1, (100, 1))
        Y_train_wrong = np.random.uniform(0, 1, (102, ))
        self.assertRaises(ValueError, self.knn_unif.fit, X_train_wrong,
                          Y_train_wrong)
        self.assertRaises(ValueError, self.knn_dist.fit, X_train_wrong,
                          Y_train_wrong)

    def test_predict_wrong_shape_fails(self):
        """
        test predicts fails if wrong shape
        """
        X_train = np.random.uniform(0, 1, (100, 5))
        Y_train = np.random.uniform(0, 1, (100, ))

        self.knn_dist.fit(X_train, Y_train)

        X_test_wrong = np.random.uniform(0, 1, (5,))

        self.assertRaises(ValueError, self.knn_dist.predict, X_test_wrong)

    def test_update_fails(self):
        """
        """
        X_train = np.random.uniform(0, 1, (100, 5))
        Y_train = np.random.uniform(0, 1, (100, ))
        self.knn_unif.fit(X_train, Y_train)

        x_new = np.random.uniform(0, 1, (1, 4))
        y_new = 0.5
        self.assertRaises(IndexError, self.knn_unif.update, x_new, y_new)

    def test_update_ok(self):
        """
        """
        X_train = np.random.uniform(0, 1, (100, 5))
        Y_train = np.random.uniform(0, 1, (100, ))
        self.knn_unif.fit(X_train, Y_train)

        x_new = np.random.uniform(0, 1, (1, 5))
        y_new = 0.5
        self.knn_unif.update(x_new, y_new)

        self.assertTrue(len(self.knn_unif._Knn__x_train) ==
                        len(self.knn_unif._Knn__y_train))
        self.assertTrue(len(self.knn_unif._Knn__x_train) == 101)

    def test_predict_output_ok(self):
        """
        assert has predict output prediction and has 5 neighbours
        """
        X_train = np.random.uniform(0, 1, (100, 5))
        Y_train = np.random.uniform(0, 1, (100, ))

        self.knn_dist.fit(X_train, Y_train)
        self.knn_unif.fit(X_train, Y_train)

        X_test = np.random.uniform(0, 1, (1, 5))

        # on dist
        val, neighbours = self.knn_dist.predict(X_test)

        self.assertIsInstance(val, float)
        self.assertTrue(len(neighbours) == 5)

        # on hist
        val, neighbours = self.knn_unif.predict(X_test)
        self.assertIsInstance(val, np.float)
        self.assertTrue(len(neighbours) == 5)

    def test_predict_output_size_2_ok(self):
        """
        assert has predict output prediction and has 5 neighbours
        """
        X_train = np.random.uniform(0, 1, (100, 5))
        Y_train = np.random.uniform(0, 1, (100, 2))

        self.knn_dist.fit(X_train, Y_train)
        self.knn_unif.fit(X_train, Y_train)

        X_test = np.random.uniform(0, 1, (1, 5))

        # on dist
        val, neighbours = self.knn_dist.predict(X_test)
        self.assertTrue(len(val) == 2)
        self.assertTrue(len(neighbours) == 5)

        # on hist
        val, neighbours = self.knn_unif.predict(X_test)
        self.assertTrue(len(val) == 2)
        self.assertTrue(len(neighbours) == 5)


if __name__ == "__main__":
    unittest.main()
