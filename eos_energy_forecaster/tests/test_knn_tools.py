# -*- coding: utf-8 -*-
"""knn.test_victron"""
import unittest
import numpy as np
import pandas as pd

from eos_energy_forecaster.knn_tools import optimize_nn, feature_selection, \
    instantiate_knn
from eos_energy_forecaster.knn import Knn


class Test_knn_tools(unittest.TestCase):
    """
    test behaviour of the knn tools element
    """

    def setUp(self):
        self.X_train = np.random.uniform(0, 1, (120, 5))
        self.Y_train = np.random.uniform(0, 1, (120,))
        self.Y_train_2 = np.random.uniform(0, 1, (120, 2))

        self.X_test = np.random.uniform(0, 1, (30, 5))
        self.Y_test = np.random.uniform(0, 1, (30,))
        self.Y_test_2 = np.random.uniform(0, 1, (30, 2))

    def test_optimize_nn_output_int(self):
        """
        test the output of optimize nn is an int >= 1
        """
        res = optimize_nn(self.X_train, self.Y_train,
                          self.X_test, self.Y_test)
        self.assertTrue(isinstance(res, int))
        self.assertTrue(res >= 1)

        res = optimize_nn(self.X_train, self.Y_train_2,
                          self.X_test, self.Y_test_2)
        self.assertTrue(isinstance(res, int))
        self.assertTrue(res >= 1)

    def test_feature_selection_output_permutation(self):
        """
        checks first output has len <= nb_features_max
        """

        res = feature_selection(self.X_train, self.Y_train,
                                self.X_test, self.Y_test,
                                max_nb_feat=4)

        self.assertTrue(len(res[0]) <= 4)
        for elem in res[0]:
            self.assertIsInstance(elem, int)

        # with two dimensions:
        res = feature_selection(self.X_train, self.Y_train_2,
                                self.X_test, self.Y_test_2,
                                max_nb_feat=4)

        self.assertTrue(len(res[0]) <= 4)
        for elem in res[0]:
            self.assertIsInstance(elem, int)

    def test_instantiate_knn(self):
        """
        checks outputs is a Knn instance, already fitted
        """

        feature_frame = pd.DataFrame(self.X_train)
        target_frame = pd.DataFrame(self.Y_train)

        knn, feats = instantiate_knn(target_frame=target_frame,
                                     features_frame=feature_frame,
                                     min_neigh=5)

        self.assertTrue(isinstance(knn, Knn))
        self.assertTrue(len(knn._Knn__x_train) > 1)
        self.assertTrue(len(knn._Knn__x_train) == len(knn._Knn__y_train))


if __name__ == "__main__":
    unittest.main()
