# pylint: disable=too-many-arguments, broad-except

"""

This scripts aims at retrieving historical wether forecast from
            darksky.net

their api seems simple: free up to 1000 call / day
 and $0.0001 afterwards

hourly request: Midnight Day of request ===> Midnight of the next day

"""

import logging

import os
import json
import datetime as dt
import pandas as pd
import psycopg2
from psycopg2 import extras

import requests

from eos_utilities.check_functions import check_type

psycopg2.extensions.register_adapter(dict, extras.Json)


KEYS = ["83379a688e6d356ad55cd82a2b06f158",  # JEREMIE
        "d70377b0bf6c3798fae84cf119e46048",  # MAXIME
        "84455176770e6e4463e4ddc296c06669",  # DIMITRI
        "79d94934d1852e4a9ea132be0e2d26f8"]  # John
BASE_STRING = "https://api.darksky.net/forecast/"
UNIT = "si"


####
#  Creating logger

LOGGER = logging.getLogger('Weather API')
LOGGER.setLevel(logging.DEBUG)
CH = logging.StreamHandler()
CH.setLevel(logging.DEBUG)
FORMATTER = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
CH.setFormatter(FORMATTER)
LOGGER.addHandler(CH)


def get_request(lat, lon, key, time, logger=LOGGER, units=UNIT):
    """
    get request
    """
    check_type(time, dt.datetime, "time")
    int_date = int(
        (time - dt.datetime(year=1970, day=1, month=1)).total_seconds())

    # format is forecast/key/[latitude],[longitude], time"

    url = BASE_STRING + key + "/" + str(lat) + "," + str(lon) + \
        "," + str(int_date) + "?units=" + units + "&lang=en"

    try:
        response = requests.get(url)
        logger.info("response with status %s", (response.status_code))

        if response.status_code == 200:
            data = response.json()
            return data

        if response.status_code == 403:
            logger.warning("key: %s might have reached its limit", key)

        return None

    except Exception as e:
        logger.warning("Not able to retrive data for day: %s", (time, e))


def make_dir(file_path, site_name, logger=LOGGER):
    """
    makes dir at file_path with name site_name
    """

    try:
        os.mkdir(file_path + str(site_name) + "/data")

    except Exception as e:
        logger.warning("Not able to create directory %s", e)

    else:
        logger.info("Directory already exists for %s",
                    file_path + str(site_name))


def save_in_base(data_dict, data_key, host, dbname,
                 user, password, weather_table, logger=LOGGER):
    """
    save data dict in database
    """
    connection = psycopg2.connect(host=host, dbname=dbname,
                                  user=user, password=password)
    cursor = connection.cursor()
    cursor.execute("INSERT INTO {} VALUES (%s, %s)".format(weather_table),
                   (data_key, data_dict))

    logger.debug("Data stocked in server")
    connection.commit()
    connection.close()


def save_in_file(obj, complete_fp, logger=LOGGER):
    """
    save data in file
    """

    with open(complete_fp, "w") as f_p:
        try:
            json.dump(obj, f_p)
        except Exception as e:
            logger.warning("unable to write data %s", e)


def process_weather_dict(weather_dict, mode="daily"):
    """  processes dict before saving into dict """
    allowed_mode = ["daily", "hourly", "all"]

    if mode == "daily":
        daily = weather_dict["daily"]["data"]
        daily[0].update({"offset": weather_dict["offset"]})
        return daily

    if mode == "hourly":
        hourly = weather_dict["hourly"]["data"]  # now it is a list

        for elem_dict in hourly:
            elem_dict.update({"offset": weather_dict["offset"]})

        return hourly

    if mode == "all":
        return [weather_dict]  # list of a dict for compatibility

    raise ValueError(
        "Mode Must be in {}, given {}".format(allowed_mode, mode))


def weather_record(lat, lon, first_date, key, last_date=None, mode="daily",
                   delta_t="1D", file_path=None, save_base=True,
                   save_file=False, logger=LOGGER, **base_info):
    """

    time are meant to be expressed in local time
    ---------------------------------------------

    lat: [float] latitude

    lon: [float] longitude

    first_date: [datetime] first (or only) date to retrieve

    mode: [str] one of "daily", "hourly" (the two frequency of data
                retrieval in darsky api)

    max_iter: [int] max number of request

    save_base: [bool] save bool in base if True information:
        **base_info: [dict] must be provided with keys:
                host, dbname, user, password, table

    save_file: [bool] wether to save the data as pickled file default False
                      if True:
                file_path [str] valid filepath must be provided

    logger: [Logger], instance of a logger
    """
    if last_date is None:
        last_date = first_date

    check_type(lat, float, 'latitude')
    check_type(lon, float, 'longitude')
    check_type(first_date, dt.datetime, 'first date of data retrieval')
    check_type(last_date, dt.datetime, 'last date of data retrieval')
    check_type(delta_t, str, 'delta_t')

    if save_file:
        check_type(file_path, str, 'file path')

    if save_base:
        host = base_info.get("host"),
        dbname = base_info.get("dbname"),
        user = base_info.get("user"),
        password = base_info.get("password"),
        weather_table = base_info.get("table")
        if any(x is None for x in
               [host, dbname, user, password, weather_table]):
            raise ValueError("at least one element of base info is None:"
                             "{}".format(base_info))

    date_range = pd.date_range(start=first_date, end=last_date, freq=delta_t)

    if len(date_range) >= 500:
        logger.info("The number of request per key is 1000,"
                    " number of requests asked > 500")

    # recall max_iter for one day and one key is 1000
    for request_date in date_range:

        overall_data = get_request(lat, lon, key, request_date,
                                   logger, units="si")
        data_list = process_weather_dict(overall_data, mode=mode)

        for data in data_list:

            if mode != "all":
                true_time = pd.to_datetime(data["time"], unit="s") + \
                    dt.timedelta(hours=data["offset"])

            else:
                true_time = pd.to_datetime(data["daily"]["data"][0]["time"],
                                           unit="s") + \
                    dt.timedelta(hours=data["offset"])

            if save_base:
                save_in_base(data_dict=data, data_key=true_time,
                             host=host, dbname=dbname, user=user,
                             password=password, weather_table=weather_table,
                             logger=LOGGER)

            if save_file:
                complete_fp = file_path + str(true_time)
                save_in_file(data, complete_fp)

        logger.debug("Data saved for day: %s", request_date)
