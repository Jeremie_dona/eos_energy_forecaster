# pylint: disable=too-many-arguments, broad-except

"""
This module contains some useful function
in order to build a training base


check_array: put the X_train and Y_train in the form

        (nb_sample, nb_features), (nb_sample, pred_dim)

        with output : x_train_reshaped, y_train_reshaped,
                      pred_dim, nb_features

"""
import datetime as dt
import numpy as np
import pandas as pd

from scipy.integrate import cumtrapz
from eos_utilities.check_functions import check_type


def check_array(x_train, y_train):
    """ check the array shape """
    check_type(x_train, np.ndarray, "x_train")
    check_type(y_train, np.ndarray, "y_train")

    shape_x = x_train.shape
    shape_y = y_train.shape

    if len(shape_x) > 2 or len(shape_y) > 2:
        raise ValueError("Shape of x or y > 2")

    nb_sample = set(shape_x).intersection(shape_y)

    if not nb_sample:
        raise ValueError("X and Y must have one dimension that matches")
    else:
        nb_sample = nb_sample.pop()
        nb_features = set(shape_x).difference({nb_sample}).pop()

        # now reshape x_train
        x_train_reshaped = x_train.reshape((nb_sample, nb_features))

        if len(shape_y) == 1 or min(shape_y) == 1:
            y_train_reshaped = y_train.reshape((nb_sample,))
            pred_dim = 1

        else:
            pred_dim = set(shape_y).difference({nb_sample}).pop()
            y_train_reshaped = y_train.reshape((nb_sample, pred_dim))

    return x_train_reshaped, y_train_reshaped, pred_dim, nb_features


def compute_integral(dataframe, integral_bounds,
                     time_label="timestamp_utc",
                     solar_label="p_solar"):
    """
    build integral bases
    """

    data = dataframe.copy()
    check_type(data, pd.core.frame.DataFrame)

    if integral_bounds[0] > integral_bounds[1]:
        raise ValueError("integral computation over two"
                         " days is not handled yet, consider"
                         " using local time")
    try:
        data[time_label] = pd.to_datetime(data[time_label])
        data.set_index(time_label, inplace=True)
        data.sort_index(inplace=True)
        data["dt"] = (data.index - data.index[0]).total_seconds().values

    except Exception as e:
        raise TypeError("Unable to convert time column to datetime,"
                        "error {}".format(e))

    # Compute cumulative integral
    integral = cumtrapz(y=data[solar_label], x=data["dt"])
    integral = np.insert(integral, 0, 0)  # insert to have same length
    data["integral"] = integral
    data["date"] = data.index.date
    data["hour"] = data.index.hour

    integral_data = {}
    unique_dates = np.unique(data["date"])

    for date in unique_dates:
        sub_d = data.loc[
            (data["date"] == date) & (data["hour"] >= integral_bounds[0]) &
            (data["hour"] < integral_bounds[1])]
        int_val = sub_d["integral"].max() - sub_d["integral"].min()
        integral_data[date] = int_val

    return integral_data


def features_computation(dataframe, integral_data,
                         time_label, solar_label,
                         hour_of_pred, weather_data=None,
                         mean_lb=6, var_lb=18):

    """
    computes (all) features over a dataframe
    """

    data = dataframe.copy()
    data_dict = {}

    for date in integral_data.keys():
        dt_pred = dt.datetime.combine(date, dt.time(hour=hour_of_pred))

        # integral of previous day
        previous_date = date - dt.timedelta(days=1)

        try:
            previous_day_int = integral_data[previous_date]
        except KeyError:
            print "previous day int not available, inputting 0"
            previous_day_int = 0

        features = elem_signal_features(data, dt_pred, time_label,
                                        solar_label, mean_lb, var_lb,
                                        previous_day_int=previous_day_int)

        if date in integral_data:
            features.update({"integral": integral_data[date]})
        else:
            print "missing integral value for date {}".format(date)

        if weather_data and date in weather_data:
            features.update(weather_data[date])

        data_dict[date] = features

    final_df = pd.DataFrame.from_dict(data_dict, orient="index")
    final_df.sort_index(inplace=True)

    return final_df


def compute_mean(data, date_of_pred, time_label, solar_label, mean_lb):
    """
    compute mean on a defined "look back"
    """
    mean_dt = data[(data[time_label] <= date_of_pred) &
                   (data[time_label] >= date_of_pred -
                    dt.timedelta(hours=mean_lb))]
    mean = np.nanmean(mean_dt[solar_label])
    return mean


def signal_variance(data, date_of_pred, time_label, solar_label, var_lb):
    """
    compute variance metrics on a defined "look back"
    """

    var_dt = data[(data[time_label] <= date_of_pred) &
                  (data[time_label] >= date_of_pred -
                   dt.timedelta(hours=var_lb))]

    var_signal = np.std(np.abs(var_dt[solar_label].diff()))

    f_diff = np.sign(var_dt[solar_label].diff()).values
    nb_var_change = f_diff[:-1] * f_diff[1:]
    nb_var_change = np.sum(nb_var_change < 0) / float(len(f_diff))

    return var_signal, nb_var_change


def elem_signal_features(data, date_time_pred, time_label, solar_label,
                         mean_lb, var_lb, previous_day_int=None):
    """
    computes elementary features for a point
    """

    check_type(data, pd.core.frame.DataFrame, "elem time series")
    check_type(date_time_pred, dt.datetime, "date_time_pred")

    # mean
    mean = compute_mean(data, date_time_pred, time_label, solar_label, mean_lb)

    # var metrics
    var_signal, nb_var_change = signal_variance(
        data, date_time_pred, time_label, solar_label, var_lb)

    features = {"mean": mean,
                "nb_var_change": nb_var_change,
                "var_signal": var_signal,
                "day_of_week": date_time_pred.isoweekday(),
                "month": date_time_pred.month}

    if previous_day_int is not None:
        features.update({"previous_day_int": previous_day_int})

    return features


def process_data(signal_dataframe, time_col, power_col, integral_bounds,
                 hour_of_pred, mean_lb=6, var_lb=15, weather_base=False):

    """
        processes data in order to be able to fit the knn algorihtm

        input:
        ------
            signal_dataframe: [pd.DataFrame] dataframe with at least
                              [time_col, power_col]

            time_col: [str] indicates the name of the time column
                            in signal_dataframe

            power_col: [str] indicates the name of the power column
                             in signal_dataframe


            integral_bounds: [list, np.ndarray, tuple]
                             (low hour, high hour) in order to compute the
                             daily integral

            hour_of_pred: [int] hour at which the prediction is made

            mean_lb: [int] look back to compute the average features

            var_lb: [int] look back to compute the variance features

            weather_base: [False or pd.DataFrame] base of columns
                        [datetime.date, features]


        output:
        -------

                x_train, y_train, columns_order

    """

    # integral and hour of pred
    if len(integral_bounds) != 2:
        raise ValueError("Integral bound must be of length 2")

    if integral_bounds[0] > integral_bounds[1]:
        raise ValueError("low bound must be < than high bounds")

    check_type(signal_dataframe, pd.core.frame.DataFrame,
               "signal_dataframe")
    check_type(integral_bounds[0], int, "low_bound")
    check_type(integral_bounds[1], int, "high_bound")
    check_type(hour_of_pred, int, "hour_of_pred")
    check_type(mean_lb, int, "mean_lb")
    check_type(var_lb, int, "var_lb")
    check_type(time_col, str, "time col")
    check_type(power_col, str, "power col")

    # Compute
    y_train_dict = compute_integral(dataframe=signal_dataframe,
                                    integral_bounds=integral_bounds,
                                    time_label=time_col,
                                    solar_label=power_col)

    # prepare array for fitting
    y_train_array = pd.DataFrame.from_dict(y_train_dict,
                                           orient="index")
    y_train_array = y_train_array.values
    y_train_array = y_train_array.reshape(len(y_train_array),)

    # set the x_train
    x_train = features_computation(
        dataframe=signal_dataframe, integral_data=y_train_dict,
        time_label=time_col, solar_label=power_col,
        hour_of_pred=hour_of_pred, weather_data=weather_base,
        mean_lb=mean_lb, var_lb=var_lb)

    column_order = x_train.columns

    # get the numpy array
    x_train = x_train.values

    return x_train, y_train_array, column_order


def update_preprocess(new_x, new_y, column_order, logger):
    """
    checks that updates will be ok
    """
    check_type(new_x, dict, "new_x")
    check_type(new_y, float, "new_y")

    # get the x's in the right format
    missing_keys = list(set(column_order).difference(set(new_x.keys())))

    if missing_keys:
        logger.warning("missing var in update: "
                       "%s, inputting nan's", missing_keys)

    for key in missing_keys:
        new_x[key] = np.nan

    new_x_arr = np.array([new_x[var] for var in column_order])

    return new_x_arr, new_y


def predict_preprocess(x_test, column_order, logger):
    """
    checks that prediction will end up nicely
    """

    check_type(x_test, dict, "new_x")

    # checks x's got the necessary features
    missing_keys = list(set(column_order).difference(set(x_test.keys())))

    if missing_keys:
        logger.warning("missing var in update: "
                       "%s, inputting nan's", missing_keys)

    for key in missing_keys:
        x_test[key] = np.nan

    x_test = np.array([x_test[var] for var in column_order])

    return x_test
