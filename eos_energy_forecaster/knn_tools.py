# pylint: disable=too-many-arguments, broad-except, too-many-instance-attributes


"""
Few tools in order to tune and train knn algorithm

"""


import logging
from itertools import combinations

import numpy as np
import pandas as pd

from eos_utilities.check_functions import check_type
from eos_energy_forecaster.utils import check_array

LOGGER = logging.getLogger('Knn optim log')
LOGGER.setLevel(logging.DEBUG)
CH = logging.StreamHandler()
CH.setLevel(logging.DEBUG)
FORMATTER = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
CH.setFormatter(FORMATTER)
LOGGER.addHandler(CH)


class Knn(object):

    """

    parameters:

        n_neighbors:
        -----------
                    [int] number of neighbors to be considered

        weights:
        --------
                    [str] one of ["uniform", "distance"]. Way of
                    averaging the neighbors:
                        uniform: regular average
                        distance: weighted average

    """

    def __init__(self, n_neighbors, weights="uniform", logger=None):
        """
        instantiate and store parameters

        """
        self._logger = logger or logging.getLogger(__name__)
        self.__weights_list = ["uniform", "distance"]

        check_type(n_neighbors, int, "n_neighbors")

        if weights not in self.__weights_list:
            raise ValueError(
                "given weights not recognized: {} handled: {}".format(
                    weights, self.__weights_list))

        self.__n_neighbors = n_neighbors
        self.__weights = weights

        # dimension attributes
        self.__nb_features = None
        self.__pred_dim = None

        # train base
        self.__x_train = None
        self.__y_train = None

    def fit(self, x_train, y_train):
        """
        stores

            - x_train:
              --------
                [numpy.ndarray] of shape: (nb_samples, nb_features)


            - y_train:
              --------
                [numpy.ndarray] of shape: (nb_samples, dimension of prediction)
        """

        x_train, y_train, pred_dim, nb_features = check_array(x_train, y_train)

        self.__x_train = x_train
        self.__y_train = y_train

        self.__pred_dim = pred_dim
        self.__nb_features = nb_features

    def __distance(self, x_test):
        """
        computes distance and returns nearest_neigh_index and their norm values
        """

        norm = np.linalg.norm(self.__x_train - x_test, axis=1)
        nn_index = np.argsort(norm)[0:self.__n_neighbors]
        norm_values = norm[nn_index]

        return nn_index, norm_values

    def update(self, new_x, new_y):
        """
        updates (x_train, y_train) with (new_x, new_y)


        - new_x: [np.ndarray] of shape (1, nb_features)
          ------

        - new_y : [np.ndarray, float] new value observed for new_x
          -------

        """

        check_type(new_x, np.ndarray, "new_x")
        check_type(new_x, (int, float, np.ndarray), "new_x")

        # check that x dimension matches
        new_x = new_x.reshape((1, max(new_x.shape)))
        if new_x.shape[1] != self.__nb_features:
            raise IndexError(
                "new_x shape is not reshapable in (1, {}) given"
                "shape: {}".format(self.__nb_features, new_x.shape))

        # check that y dimension matches
        if self.__pred_dim == 1:
            if isinstance(new_y, np.ndarray):
                new_y = new_y.reshape((1,))
            else:
                new_y = np.array([np.float(new_y)])
        else:
            new_y = new_y.reshape((1, self.__pred_dim))

        # update basis
        try:
            self.__y_train = np.append(self.__y_train, new_y, axis=0)
            self.__x_train = np.append(self.__x_train, new_x, axis=0)
        except Exception as e:
            self._logger.warning(
                "Unable to update x_train and y_train: %s", e)

    def predict(self, x_test):
        """
        predict y_test according to x_test

            - x_test:
              -------
                [np.ndarray] of shape (1, nb_features)
        """

        # Check the array
        nb_features = self.__x_train.shape[1]

        if x_test.shape != (1, nb_features):
            raise ValueError("x_test must be of shape (1, {})"
                             " given: {}".format(nb_features, x_test.shape))

        # find nearest neighbors
        nn_index, norm_values = self.__distance(x_test)
        nearest_neigh = self.__y_train[nn_index]

        # average the neighbours
        if self.__weights == "uniform":
            weights = None

        elif self.__weights == "distance":
            weights = norm_values / np.sum(norm_values)

        prediction = np.average(nearest_neigh, axis=0, weights=weights)

        return prediction, nearest_neigh


def optimize_nn(x_train, y_train, x_test, y_test, n_limit=(2, 20), min_nn=5):
    """
    optimize the number of the nearest neighbors

    input:
    ------

        - x_train : [np.ndarray] (nb_samples, nb_features)

        - y_train : [np.ndarray] (nb_samples, pred_dim)

        - x_test : [np.ndarray] (nb_samples, nb_features)

        - y_train : [np.ndarray] (nb_samples, pred_dim)

    """
    av_err = []

    assert n_limit[0] < n_limit[1]

    nns = np.arange(n_limit[0], n_limit[1], 1)
    for n_n in nns:
        model = Knn(n_n)
        model.fit(x_train, y_train)
        err = []

        for j in range(len(x_test)):
            elem_x_test = x_test[j, :].reshape((1, x_test.shape[1]))
            y_hat = model.predict(elem_x_test)[0]
            elem_err = y_hat - y_test[j]
            err.append(elem_err)

        av_err.append(np.mean(np.abs(err)))

    return max(nns[np.argmin(av_err)], min_nn)


def feature_selection(x_train, y_train, x_test,
                      y_test, max_nb_feat, nb_neigh=15):

    """ selects the best combination of features

    input:
    ------

        - x_train : [np.ndarray] (nb_samples, nb_features)

        - y_train : [np.ndarray] (nb_samples, pred_dim)

        - x_test : [np.ndarray] (nb_samples, nb_features)

        - y_train : [np.ndarray] (nb_samples, pred_dim)

        - nb_neigh : [int] number of neigh for feature selection

    """
    best_score = np.inf
    best_combination = None
    all_feat = range(x_train.shape[1])

    for nb_features in range(2, max_nb_feat):

        for features in combinations(all_feat, nb_features):

            # define model
            model = Knn(n_neighbors=nb_neigh, weights="uniform")
            features = list(features)
            model.fit(x_train[:, features], y_train)

            # retrieve preds
            preds = []
            for j in range(len(x_test)):
                elem_x_test = x_test[j, features].reshape(1, len(features))
                preds.append(model.predict(elem_x_test)[0])
            preds = np.array(preds)

            # compute Relative mean absolute error
            if len(y_test.shape) == 1 or min(y_test.shape) <= 1:
                non_0 = y_test != 0
                err = np.abs(preds[non_0] - y_test[non_0]) / \
                    np.ravel(y_test[non_0])

                err = np.mean(err)

            else:
                err = np.mean(np.abs(preds - y_test))

            if err < best_score:
                best_score = err
                best_combination = sorted(features)

    return best_combination, best_score


def instantiate_knn(features_frame, target_frame, min_neigh,
                    weights="uniform"):
    """
    function that instantiate a knn using prebuilt features
    and that optimizes the choice of features
    """

    check_type(features_frame, pd.core.frame.DataFrame, "dataframe")
    check_type(target_frame, pd.core.frame.DataFrame, "dataframe")
    check_type(min_neigh, int, "min number of neighbors")

    # check size
    if not bool(set(features_frame.shape).intersection(target_frame.shape)):
        raise IndexError("shape of features: {} and y_train"
                         "does not fit: {}".format(features_frame.shape,
                                                   target_frame.shape))

    # Split and test
    cut = int(0.75 * len(target_frame))
    x_train = features_frame.iloc[:cut].values
    x_test = features_frame.iloc[cut:].values
    y_train = np.ravel(target_frame.iloc[:cut].values)
    y_test = np.ravel(target_frame[cut:].values)

    # first features selection
    selected_features, _ = feature_selection(x_train, y_train, x_test, y_test,
                                             max_nb_feat=8, nb_neigh=min_neigh)

    # then fitting the good number of neighbors
    best_nn = optimize_nn(x_train, y_train, x_test, y_test, n_limit=(2, 20))

    knn = Knn(max(best_nn, min_neigh), weights=weights)

    x_train = features_frame[selected_features].values
    y_train = target_frame.values.reshape((len(target_frame,)))

    knn.fit(x_train, y_train)

    return knn, selected_features
