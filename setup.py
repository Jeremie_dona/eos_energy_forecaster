#!/usr/bin/env python
"""eos_production_forecaster setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

from setuptools import setup, find_packages

setup(
    # Module description
    name='eos_energy_forecaster',
    version='0.0.1',
    description='provides a forecast for energy production using weather data',

    # Author
    author='The eLum Energy dev team',
    author_email='contact@elum-energy.com',

    # providing packages
    packages=find_packages(),

    # packages data
    package_data={'eos_energy_forecaster': ['test/*.csv']},

    # required packages
    install_requires=[
        'pandas>0.19.1,<0.21',
        'numpy>=1.12,<1.13',
        'eos_test',
        'eos_utilities'
    ],

    # Testing
    test_suite='eos_energy_forecaster.load_tests'
)
